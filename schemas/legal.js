import { MdOutlineDriveFolderUpload } from "react-icons/md"

export default {
    name: 'legal',
    title: 'Legal',
    type: 'document',
    icon: MdOutlineDriveFolderUpload,
    fields: [
        {
            name: 'name',
            type: 'string',
            title: 'Name',
        },
        {
            name: 'type',
            type: 'reference',
            title: 'Type',
            to: { type: 'legalType' }
        },
        {
            name: 'level',
            type: 'reference',
            title: 'Level',
            to: { type: 'legalLevel' }
        },
        {
            name: 'file',
            type: 'file',
            title: 'File',
            storeOriginalFilename: true
        }
    ],
}