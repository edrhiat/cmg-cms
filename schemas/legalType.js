import { MdOutlineTableChart } from "react-icons/md"

export default {
    name: 'legalType',
    title: 'Legal Type',
    type: 'document',
    icon: MdOutlineTableChart,
    fields: [
        {
            name: 'name',
            type: 'string',
            title: 'Name',
        },
    ],

}