import { MdAutoGraph } from "react-icons/md"

export default {
    name: 'legalLevel',
    title: 'Legal Level',
    type: 'document',
    icon: MdAutoGraph,
    fields: [
        {
            name: 'name',
            type: 'string',
            title: 'Name',
        },
    ],

}